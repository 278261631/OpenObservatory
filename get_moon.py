from time import strftime

import numpy as np
import astroplan as as_plan
import astropy.units as u
import pylunar
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time, TimezoneInfo

import time as systime
from datetime import datetime
from astropy.coordinates import solar_system_ephemeris, EarthLocation, AltAz, get_sun
from astropy.coordinates import get_body_barycentric, get_body, get_moon
from astropy.utils import iers
import json
import redis
import stomp

iers.conf.auto_download = False



while True:
    # print(EarthLocation.get_site_names())
    # full moon 0.999
    # t = Time("2018-03-2 0:25")
    # new moon 0.001
    # t = Time("2018-03-17 9:25")
    # print(t)
    t = Time.now()
    # print(t)
    # loc = EarthLocation.of_site('greenwich')
    # (3980608.9024681724, -102.47522910648239, 4966861.273100675) m
    #loc = EarthLocation(3980608.9024681724, -102.47522910648239, 4966861.273100675, unit='m')
    lon_gyz = 88.12389167
    lat_gyz = 47.7819167
    height_gyz = 815
    loc_gyz = EarthLocation.from_geodetic(lon_gyz, lat_gyz, height_gyz)

    moon = get_moon(t, loc_gyz)
    sun_now = get_sun(t)
    moon_altaz = moon.transform_to(AltAz(obstime=t, location=loc_gyz))
    sun_altaz = sun_now.transform_to(AltAz(obstime=t, location=loc_gyz))

    observer_alt = as_plan.Observer(location=loc_gyz, timezone='Asia/Shanghai')
    sun_rise = observer_alt.sun_rise_time(time=t, which='next')
    moon_rise = observer_alt.moon_rise_time(time=t, which='next')
    moon_illumination = as_plan.moon.moon_illumination(time=t)
    # moon_age = as_plan.moon.age()
    # moon_age = as_plan.moon.moon_phase_angle(t)
    # print(moon_age)
    pylunar_moon = pylunar.MoonInfo((43, 50, 30), (87, 36, 00))
    systime_utc_now = datetime.utcnow()
    # pylunar_moon.update((systime_utc_now.year,systime_utc_now.month,systime_utc_now.day))
    pylunar_moon.update((systime_utc_now.year, systime_utc_now.month, systime_utc_now.day,systime_utc_now.hour,systime_utc_now.minute))
    print(systime_utc_now)
    print(pylunar_moon.age())

    utc_plus_8_hour = TimezoneInfo(utc_offset=8*u.hour)
    # print(sun_rise.to_datetime(timezone=utc_plus_8_hour))
    # print(moon_rise.to_datetime(timezone=utc_plus_8_hour))

    # print(t.to_datetime(timezone=utc_plus_8_hour))
    print(systime.strftime("%Y-%m-%d %H:%M:%S", systime.localtime()))
    print(json.dumps({'sun_rise': str(sun_rise.to_datetime(timezone=utc_plus_8_hour)), 'moon_illumination': str(moon_illumination),
                      'moon_ra_deg': '{0.ra.deg:.8f}'.format(moon), 'moon_dec_deg': '{0.dec.deg:.8f}'.format(moon), 'moon_alt_deg': '{0.alt.deg:.2f}'.format(moon_altaz), 'sun_alt_deg': '{0.alt.deg:.2f}'.format(sun_altaz)
                      }))
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    r.set('MoonSunStatusUpdateTime', systime.strftime("%Y-%m-%d %H:%M:%S", systime.localtime()))
    gyz_north = SkyCoord(alt=0, az=0, obstime=t, frame='altaz', location=loc_gyz, unit='deg')
    gyz_east = SkyCoord(alt=0, az=90, obstime=t, frame='altaz', location=loc_gyz, unit='deg')
    gyz_south = SkyCoord(alt=0, az=180, obstime=t, frame='altaz', location=loc_gyz, unit='deg')
    gyz_west = SkyCoord(alt=0, az=270, obstime=t, frame='altaz', location=loc_gyz, unit='deg')
    gyz_top = SkyCoord(alt=90, az=90, obstime=t, frame='altaz', location=loc_gyz, unit='deg')
    gyzN = gyz_north.icrs
    gyzE = gyz_east.icrs
    gyzS = gyz_south.icrs
    gyzW = gyz_west.icrs
    gyzT = gyz_top.icrs
    print('{0.ra.deg:.3f}'.format(gyzN)+'    '+'{0.dec.deg:.3f}'.format(gyzN))
    print('{0.ra.deg:.3f}'.format(gyzE)+'    '+'{0.dec.deg:.3f}'.format(gyzE))
    print('{0.ra.deg:.3f}'.format(gyzS)+'    '+'{0.dec.deg:.3f}3'.format(gyzS))
    print('{0.ra.deg:.3f}'.format(gyzW)+'    '+'{0.dec.deg:.3f}'.format(gyzW))
    print('{0.ra.deg:.3f}'.format(gyzT)+'    '+'{0.dec.deg:.3f}'.format(gyzT))
    # 也许要用java版本的重新写一下redis

    # todo 保存毫秒值 用于计算
    r.set('MoonSunStatusString', json.dumps([
        {'site_id': '1', 'site_name': '高崖子', 'moon_age': pylunar_moon.age(), 'lon': lon_gyz, 'lat': lat_gyz, 'height': height_gyz, 'sun_rise': str(sun_rise.to_datetime(timezone=utc_plus_8_hour)), 'moon_illumination': str(moon_illumination),'moon_ra_deg': '{0.ra.deg:.8f}'.format(moon), 'moon_dec_deg': '{0.dec.deg:.8f}'.format(moon)
            , 'moon_alt_deg': '{0.alt.deg:.2f}'.format(moon_altaz), 'sun_alt_deg': '{0.alt.deg:.2f}'.format(sun_altaz)
            , 'siteNRa': '{0.ra.deg:.3f}'.format(gyzN), 'siteNdec': '{0.dec.deg:.3f}'.format(gyzN)
            , 'siteERa': '{0.ra.deg:.3f}'.format(gyzE), 'siteEdec': '{0.dec.deg:.3f}'.format(gyzE)
            , 'siteSRa': '{0.ra.deg:.3f}'.format(gyzS), 'siteSdec': '{0.dec.deg:.3f}'.format(gyzS)
            , 'siteWRa': '{0.ra.deg:.3f}'.format(gyzW), 'siteWdec': '{0.dec.deg:.3f}'.format(gyzW)
            , 'siteTRa': '{0.ra.deg:.3f}'.format(gyzT), 'siteTdec': '{0.dec.deg:.3f}'.format(gyzT)
         },
        {'site_id': '2', 'site_name': '000002', 'moon_age': pylunar_moon.age(), 'lon': lon_gyz, 'lat': lat_gyz, 'height': height_gyz, 'sun_rise': str(sun_rise.to_datetime(timezone=utc_plus_8_hour)), 'moon_illumination': str(moon_illumination),'moon_ra_deg': '{0.ra.deg:.8f}'.format(moon), 'moon_dec_deg': '{0.dec.deg:.8f}'.format(moon), 'moon_alt_deg': '{0.alt.deg:.2f}'.format(moon_altaz), 'sun_alt_deg': '{0.alt.deg:.2f}'.format(sun_altaz)}
    ]))

    siteJson = json.dumps({'msgType': 'sites', 'sites': [
        {'site_id': '1', 'site_name': '高崖子', 'moon_age': pylunar_moon.age(), 'lon': lon_gyz, 'lat': lat_gyz, 'height': height_gyz, 'sun_rise': str(sun_rise.to_datetime(timezone=utc_plus_8_hour)), 'moon_illumination': str(moon_illumination),'moon_ra_deg': '{0.ra.deg:.8f}'.format(moon), 'moon_dec_deg': '{0.dec.deg:.8f}'.format(moon)
            , 'moon_alt_deg': '{0.alt.deg:.2f}'.format(moon_altaz), 'sun_alt_deg': '{0.alt.deg:.2f}'.format(sun_altaz)
            , 'siteNRa': '{0.ra.deg:.3f}'.format(gyzN), 'siteNdec': '{0.dec.deg:.3f}'.format(gyzN)
            , 'siteERa': '{0.ra.deg:.3f}'.format(gyzE), 'siteEdec': '{0.dec.deg:.3f}'.format(gyzE)
            , 'siteSRa': '{0.ra.deg:.3f}'.format(gyzS), 'siteSdec': '{0.dec.deg:.3f}'.format(gyzS)
            , 'siteWRa': '{0.ra.deg:.3f}'.format(gyzW), 'siteWdec': '{0.dec.deg:.3f}'.format(gyzW)
            , 'siteTRa': '{0.ra.deg:.3f}'.format(gyzT), 'siteTdec': '{0.dec.deg:.3f}'.format(gyzT)
         },
        {'site_id': '2', 'site_name': '000002', 'moon_age': pylunar_moon.age(), 'lon': lon_gyz, 'lat': lat_gyz, 'height': height_gyz, 'sun_rise': str(sun_rise.to_datetime(timezone=utc_plus_8_hour)), 'moon_illumination': str(moon_illumination),'moon_ra_deg': '{0.ra.deg:.8f}'.format(moon), 'moon_dec_deg': '{0.dec.deg:.8f}'.format(moon), 'moon_alt_deg': '{0.alt.deg:.2f}'.format(moon_altaz), 'sun_alt_deg': '{0.alt.deg:.2f}'.format(sun_altaz)}
    ]})
    conn_send = stomp.Connection10([('msg.xtelescope.net', 61613)],auto_content_length=False)
    conn_send.start()
    conn_send.connect([('msg.xtelescope.net', 61613)], wait=True)
    #conn_send.send('/topic/equipment_status', body=json_process_string, headers={'type': 'textMessage'})
    conn_send.send('/topic/equipment_status', body=siteJson, headers={'type': 'textMessage'})
    # conn_send.send('/topic/equipment_status', json_center_string, conn_send)
    conn_send.disconnect()

    systime.sleep(5)
