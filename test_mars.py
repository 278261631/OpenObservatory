from astropy.time import Time
from astropy.coordinates import solar_system_ephemeris, EarthLocation
from astropy.coordinates import get_body_barycentric, get_body, get_moon
t = Time("2018-02-20 20:5")
loc = EarthLocation.of_site('greenwich')
moon = get_moon(t, loc)
moon.to_string('hmsdms')
print("-----")
