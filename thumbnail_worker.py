# coding: utf-8
import os
import logging
import threading
import time
from openastro.image import Image
import shutil
########################################
import stomp
import time
import base64
from datetime import datetime
import json
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, LoggingEventHandler
from watchdog.observers.api import ObservedWatch
########################################

class FitToThumbnail(FileSystemEventHandler):

    def __init__(self, config="config.json", logger_name="auto_thumbnail"):
        self.cfg = self.load_config(config=config)
        self.logger = logging.getLogger(logger_name)

        self.fits_root_path = self.cfg["fits_root_path"]        # 未处理fit路径
        # self.thumbnail_root_path = self.cfg["thumbnail_root_path"]  # 存放缩略图路径
        self.thumbnail_path = self.cfg["thumbnail_path"]  # 存放缩略图路径
        self.thumbnail_center_path = self.cfg["thumbnail_center_path"]  # 存放缩略图路径
        self.thumbnail_width = self.cfg["thumbnail_width"]
        self.thumbnail_height = self.cfg["thumbnail_height"]
        self.bayer_pattern = self.cfg["bayer_pattern"]
        self.processed_fits_path = self.cfg["processed_fits_path"]  # 处理完毕后fit图片的存放路径

        # TODO: 暂时没有添加文件日志句柄
        self.logfile = self.cfg["logfile"]
        self.loglevel = self.cfg["loglevel"]

        """
        将logging设置为DEBUG，用于调试，生产环境可以设置为INFO甚至WARNING
        """
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s - %(lineno)d - %(message)s'
        )
        self.logger.setLevel(logging.DEBUG)
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

        self.logger.info(
            "Config Loaded: {}".format(self.cfg)
        )

    def load_config(self, config="config.json"):
        import json
        with open(config, encoding='utf-8') as f:
            self.cfg = json.load(f)
        return self.cfg

    def walk_dirs(self, path="."):
        #TODO 替换成事件触发
        for root, dirs, files in os.walk(path):
            for name in files:
                image_path = os.path.join(root, name)
                print(image_path)
                self.image_handler(image_path)
    def event_path(self, image_path):
        print(image_path)
    def image_handler(self, image_path):
        directory, image_name = os.path.split(image_path)
        image = None
        try:
            image = Image(directory, image_name)
        except Exception as ex:
            time.sleep(2)
            print(image_path + "  --   except " + str(ex))
            self.image_handler(image_path)
            return
        image.bayer_to_rgb(
            self.bayer_pattern
        )
        img_backup = image.image_data
        image.resize(
            width=self.thumbnail_width,
            height=self.thumbnail_height
        ).save_to(
            # directory=self.thumbnail_root_path,
            directory=self.thumbnail_path,
            image_name=os.path.splitext(image_name)[0] + "_thumbnail.png"
        )
        image.image_data = img_backup
        image.center_crop(
            self.thumbnail_width,
            self.thumbnail_height
        ).save_to(
            # directory=self.thumbnail_root_path,
            directory=self.thumbnail_center_path,
            image_name=os.path.splitext(image_name)[0] + "_thumbnail_center_crop.png"
        )
        image.close_file()
        # shutil.move(image_path, self.processed_fits_path)
        # ###########TODO 此处移动文件可能造成ACP错误 无法生成ACP缩略图
        # ###########TODO 此循环读取文件可能造成本程序错误 文件刚刚被创建 尚未完全写入数据，此时转换会导致程序退出

        #  ####################################### TODO
        conn_send = stomp.Connection10([('msg.xtelescope.net', 61613)],auto_content_length=False)
        conn_send.start()
        conn_send.connect([('msg.xtelescope.net', 61613)], wait=True)
        # thumb_path = os.path.join(self.thumbnail_root_path, os.path.splitext(image_name)[0] + "_thumbnail.png")
        # center_path = os.path.join(self.thumbnail_root_path, os.path.splitext(image_name)[0] + "_thumbnail_center_crop.png")
        # thumb_path = os.path.join(self.thumbnail_path, os.path.splitext(image_name)[0] + "_thumbnail.png")
        # center_path = os.path.join(self.thumbnail_center_path, os.path.splitext(image_name)[0] + "_thumbnail_center_crop.png")
        # print(thumb_path)
        # print(center_path)
        # thumb_f = open(thumb_path, 'rb')   # 二进制方式打开图文件
        # center_f = open(center_path, 'rb')
        # thumb_base64 = base64.b64encode(thumb_f.read()).decode()  # 读取文件内容，转换为base64编码
        # center_base64 = base64.b64encode(center_f.read()).decode()  # 读取文件内容，转换为base64编码
        # thumb_f.close()
        # center_f.close()
        # json_message = {"image": str(img.tolist()).encode('base64')}
        #json_process_string = '{"messageTime":"' + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '","msgType":"数据文件处理","deviceName":"ALT-zsy","deviceColor":"#FFAA00","cameraImage":"","messageColor":"green","fitsInJpegImage":""}'
        #conn_send.send('/topic/euipment_status', body=json_process_string, headers={'type': 'textMessage'})
        # json_thumb_string = '{"messageTime":"' + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '","msgType":"预览图","deviceName":"ALT-zsy","deviceColor":"#FFAA00","cameraImage":"","messageColor":"green","fitsInJpegImage":"' + thumb_base64 + '"}'
        # json_center_string = '{"messageTime":"' + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '","msgType":"中心图","deviceName":"ALT-zsy","deviceColor":"#FFAA00","cameraImage":"","messageColor":"green","fitsInJpegImage":"' + center_base64 + '"}'
        # conn_send.send('/topic/euipment_status', body=json_thumb_string, headers={'type': 'textMessage'})
        # conn_send.send('/topic/euipment_status', json_center_string, conn_send)
        conn_send.disconnect()
        # ####################################### TODO
        self.logger.info("{} processed".format(image_path))

    def check_config(self, config="config.json"):
        # 读取配置
        while True:
            time.sleep(3)
            try:
                self.cfg = self.load_config(config=config)
                thumbnail_width = self.cfg["thumbnail_width"]
                thumbnail_height = self.cfg["thumbnail_height"]
                fits_root_path = self.cfg["fits_root_path"]
                # thumbnail_root_path = self.cfg["thumbnail_root_path"]
                thumbnail_path = self.cfg["thumbnail_path"]
                thumbnail_center_path = self.cfg["thumbnail_center_path"]
                processed_fits_path = self.cfg["processed_fits_path"]
                if not os.path.isdir(fits_root_path):
                    self.logger.error(
                        "fits_root_path error: {}.\n"
                        "Please check your config file:".format(fits_root_path)
                    )
                elif fits_root_path != self.fits_root_path:
                    self.logger.info(
                        "fits_root_path changed from {} to {}".format(self.fits_root_path, fits_root_path)
                    )
                    self.fits_root_path = fits_root_path

                if not os.path.isdir(processed_fits_path):
                    self.logger.error(
                        "fits_root_path error: {}.\n"
                        "Please check your config file:".format(processed_fits_path)
                    )
                elif processed_fits_path != self.processed_fits_path:
                    self.logger.info(
                        "processed_fits_path changed from {} to {}".format(self.processed_fits_path, processed_fits_path)
                    )
                    self.processed_fits_path = processed_fits_path

                # if not os.path.isdir(thumbnail_root_path):
                #     self.logger.error(
                #         "thumbnail_root_path error: {}.\n"
                #         "Please check your config file:".format(thumbnail_root_path)
                #     )
                # elif thumbnail_root_path != self.thumbnail_root_path:
                #     self.logger.info(
                #         "thumbnail_root_path changed from {} to {}".format(self.thumbnail_root_path, thumbnail_root_path)
                #     )
                #     self.thumbnail_root_path = thumbnail_root_path

                if not os.path.isdir(thumbnail_path):
                    self.logger.error(
                        "thumbnail_path error: {}.\n"
                        "Please check your config file:".format(thumbnail_path)
                    )
                elif thumbnail_path != self.thumbnail_path:
                    self.logger.info(
                        "thumbnail_path changed from {} to {}".format(self.thumbnail_path, thumbnail_path)
                    )
                    self.thumbnail_path = thumbnail_path

                if not os.path.isdir(thumbnail_center_path):
                    self.logger.error(
                        "thumbnail_center_path error: {}.\n"
                        "Please check your config file:".format(thumbnail_center_path)
                    )
                elif thumbnail_center_path != self.thumbnail_center_path:
                    self.logger.info(
                        "thumbnail_center_path changed from {} to {}".format(self.thumbnail_center_path, thumbnail_center_path)
                    )
                    self.thumbnail_center_path = thumbnail_center_path

                if thumbnail_width != self.thumbnail_width:
                    self.logger.info(
                        "thumbnail_width changed from: {} to {}".format(self.thumbnail_width, thumbnail_width)
                    )
                    self.thumbnail_width = thumbnail_width

                if thumbnail_height != self.thumbnail_height:
                    self.logger.info(
                        "thumbnail_height changed from:{} to {}".format(
                            self.thumbnail_height,
                            thumbnail_height
                        )
                    )
                    self.thumbnail_height = thumbnail_height

            except Exception as e:
                self.logger.error(e)

    def start(self,):
        # 开启一个线程轮询config，如果有变化会更新设置
        thread_config = threading.Thread(target=self.check_config, args=("config.json",), daemon=True)
        thread_config.start()
        # while True:
        #     self.walk_dirs(self.fits_root_path)
        #     time.sleep(5)
        event_handler1 = FitToThumbnail()
        observer = Observer()
        # watch = observer.schedule(event_handler1, path='D:\OpenObservatory', recursive=True)
        watch = observer.schedule(event_handler1, path=self.fits_root_path, recursive=True)

        logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        # event_handler2 = LoggingEventHandler()
        event_handler2 = LoggingEventHandler()
        observer.add_handler_for_watch(event_handler2, watch)      #为watch新添加一个event handler
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()

    def on_created(self, event):
        print("log file %s changed!" % event.src_path)
        event_directory, event_image_name = os.path.split(event.src_path)
        # print(event_directory)
        # print(event_image_name)
        # print(self.fits_root_path)
        if event_directory != self.fits_root_path:
            return
        if event_image_name.endswith('.fits') or event_image_name.endswith('.fit') or event_image_name.endswith('.fts'):
            self.image_handler(event.src_path)


if __name__ == '__main__':
    # 设置屏幕输出句柄
    worker = FitToThumbnail(config="config.json", logger_name="auto_thumbnail")
    worker.start()

